import React from 'react'
import Input from '../components/Input'
import Button from '../components/Button'
import Choice from '../components/Choice'
import Nav from '../components/Nav'


export default function CreateRoom() {
  return (
    <div className='container container-room-create'>
      
      <div className='row justify-content-center' style={{margin:"0 auto",position:"relative"}}>
          
          <div className='col-lg-9 blur wrap-make-room d-flex align-items-center justify-content-center flex-column'>
          <h1 className='font-weight-bold'>CREATE NEW ROOM</h1>
          <Input placeholder="Input your fancy room name here" newClass="text-center"/>

          <h3 className='font-weight-bold' style={{marginTop:"30px"}}>YOUR CHOICE HERE</h3>
          <div className='d-flex flex-row justify-content-center'>
            <Choice value="Rock"/>
            <Choice value="Paper"/>
            <Choice value="Scissors"/>
          </div>
          
          <Button addClass="button-auto" title="SAVE"/>
          
        </div>
      </div>
    </div>
    
  )
}
