import Button from "../components/Button";
import Input from "../components/Input";
import Label from "../components/Label";

const Register = () => {
 

 return(
  
  <div className="row-wrap">
   <h2 className="h2">Registration</h2>
   <form>
    <div className="d-flex flex-column mt30">
        <Label text="Username"/>
        <Input type="text" placeholder="Username"/>
    </div>
    <div className="d-flex flex-column mt30">
        <Label text="Email"/>
        <Input type="text" placeholder="Email"/>
    </div>
    <div className="d-flex flex-column mt30">
        <Label text="Password"/>
        <Input type="password" placeholder="Password"/>
    </div>

    <Button title="Register"/>
   </form>
   
   
  </div>
 
 );

}

export default Register;

