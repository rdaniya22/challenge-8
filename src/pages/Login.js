import React, { useState, useEffect } from 'react';
import Button from '../components/Button';
import Input from '../components/Input';
import Label from '../components/Label';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [accessToken, setAccessToken] = useState('');

  const navigate = useNavigate();

  const handleUsernameChange = (event) => {
    const newUserName = event.target.value;
    setUserName(newUserName);
  };

  const handlePasswordChange = (event) => {
    const newPassword = event.target.value;
    setPassword(newPassword);
  };

  
  

  const handleLogin = (event) => {
    event.preventDefault(); // Prevent the default form submission behavior

    const newAccessToken = `${password}${userName}`;
    setAccessToken(newAccessToken);//save accessToken in variable
    //save accessToken in localStorage
    localStorage.setItem('accessToken', newAccessToken);
    navigate("/user");//direct to /user page
  };

  

  return (
    <div className="row-wrap">
      <h2 className="h2">Login</h2>

      <form onSubmit={handleLogin}> {/* Attach handleLogin to the form's onSubmit event */}
        <div className="d-flex flex-column mt30">
          <Label text="Username" />
          <Input
            type="text"
            placeholder="Username"
            onChange={handleUsernameChange}
          />
        </div>

        <div className="d-flex flex-column mt30">
          <Label text="Password" />
          <Input
            type="password"
            placeholder="Password"
            onChange={handlePasswordChange}
          />
        </div>
        

        <Button title="Login" type="submit" /> {/* Change the button type to submit */}
      </form>
    </div>
  );
};

export default Login;
