import React from 'react'
import Choice from '../components/Choice'

export default function InsideRoom(props) {
  return (
    <div>
      <h1 className="font-weight-bold" style={{textAlign:"center",marginBottom:"30px"}}>{props.titleRoom}</h1>
      <div className='blur div-room d-flex flex-row'>
        <div>
            <h4 className='font-weight-bold' style={{textAlign:"center"}}>{props.player_1}</h4>
            <Choice value="Rock"/>
            <Choice value="Paper"/>
            <Choice value="Scissors"/>
        </div>
        <div className='d-flex align-items-center justify-content-center'>
          <h3 className="font-weight-bold" style={{fontSize:"20px"}}>VS</h3>
        </div>
        <div>
            <h4 className='font-weight-bold' style={{textAlign:"center"}}>{props.player_2}</h4>
            <Choice value="Rock"/>
            <Choice value="Paper"/>
            <Choice value="Scissors"/>
        </div>
        

      </div>
      
    </div>
  )
}
