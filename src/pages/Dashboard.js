import React from 'react'
import Nav from '../components/Nav'
import { Link } from 'react-router-dom'
import harryImg from '../img/harry.png';
import herImg from '../img/hermione.png';
import ronImg from '../img/ron.png';
import dracoImg from '../img/draco.png';
import cedricImg from '../img/cedric.png';
import gonagalImg from '../img/gonagal.png';


export default function Dashboard() {
  return (
    <div style={{width:"100%",height:"100vh"}}>
      <Nav/>
      <div className='container' style={{height:"100%"}}>
        <div className='row' style={{height:"100%"}}>
          <div className='col-4'>
            <div className='blur wrap-side d-flex flex-column' style={{height:"80%"}}>
              <ul>
                <li><Link to={"/user/profile"}>
                My Detail
              </Link></li>
                <li><Link to={"/create-room"}>
                Create Room
              </Link></li>
                <li><Link to={"/user/history"}>
                  Game History
                </Link></li>
              </ul>
              
              
              

            </div>
            
          </div>
          <div className='col-8'>
            <div className='blur d-flex flex-row justify-content-between' style={{padding:"40px",borderRadius:"30px",gap:"20px",flexWrap:"wrap"}}>
              <div>
              <img src={harryImg} alt="Harry Potter" height="250"/>
              </div>
              
              <div>
              <img src={herImg} alt="Hermione" height="250"/>
              </div>

              <div>
              <img src={ronImg} alt="Ron" height="250"/>
              </div>

              <div>
              <img src={dracoImg} alt="Ron" height="250"/>
              </div>

              <div>
              <img src={gonagalImg} alt="Cedric" height="250"/>
              </div>

              <div>
              <img src={cedricImg} alt="Cedric" height="250"/>
              </div>

             
              

            </div>

          </div>

        </div>

      </div>
      
    </div>
  )
}
