import React from 'react';
import { useState } from "react";
import harryImg from '../img/harry.png';
import BoxRoom from '../components/BoxRoom';




export default function Player() {
  const [biodata,setBiodata] = useState([
    {
      user_id:"1",
      userName: "harry_potter",
      firstName : "Harry",
      lastName : "Potter",
      school : "Hogwarts",
      birthDate : "31 Juli 1980",
      location : "London"
    }
  ]);
  const [rooms,setRooms] = useState([
    {
      room_id:"1",
      roomName: "Room 1",
      winner: "ron_weasley"
    },
    {
      room_id:"2",
      roomName: "Room 2",
      winner: "draco_malfoy"
    },
    {
      room_id:"3",
      roomName: "Room 3",
      winner: "sirius_black"
    },
    {
      room_id:"4",
      roomName: "Room 4",
      winner: "ginny_weasley"
    },
    {
      room_id:"5",
      roomName: "Room 5",
      winner: "helga_hufflepuf"
    },
    {
      room_id:"6",
      roomName: "Room 6",
      winner: "fleur_07"
    }
  ]);
  return (
    <div className='container-fluid'>
      <div className='row player-wrap justify-content-between'>
          <div className='col-6'>
              <div className='wrap-title blur'>
                  <h2 className='font-weight-bold' style={{marginBottom:"0"}}>PLAY VS COM</h2>

                  
              </div>
              <div className='wrap-room d-flex flex-row flex-wrap'>
              
              {rooms.map((aRoom) =>(
                <BoxRoom roomNumber={aRoom.roomName} winner={aRoom.winner}/>
              ))}

              </div>
              
          </div>
          <div className='col-4'>
            <div className='wrap-right d-flex justify-content-center flex-column align-items-center'>
                <img src={harryImg} alt="avatar" width="200"/>
                <h2 className='font-weight-bold' style={{paddingTop:"30px"}}>{biodata[0].firstName} {biodata[0].lastName}</h2>

                <p>Username: {biodata[0].userName}</p>
                <p>Birthdate: {biodata[0].birthDate}</p>
                <p>Location: {biodata[0].location}</p>
                <p>School: {biodata[0].school}</p>
            </div>
          </div>

      </div>
      

    </div>
  )
}
