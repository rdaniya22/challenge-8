import React from 'react';
import Nav from '../components/Nav';
import Login from './Login';
import Register from './Register';

export default function Landing({ page }) {
  let childComponent;

  if (page === 'register') {
    childComponent = <Register/>;
  } else {
    childComponent = <Login/>;
  }

  return (
    <div className='d-flex align-items-center flex-column' style={{ height: "100vh", width: "100%" }}>
      <Nav />
      {childComponent}
    </div>
  );
}
