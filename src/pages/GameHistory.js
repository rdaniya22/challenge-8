import React from 'react';
import History from '../components/History';
import { useState } from "react";
import historyImg from '../img/history.png';

export default function GameHistory() {
  const [histories,setHistories] = useState([
    {
      id:"1",
      rival: "VS Computer",
      date: "10 May 2023 17:58",
      result: "win"
    },
    {
      id:"2",
      rival: "VS Computer",
      date: "11 May 2023 19:58",
      result: "lost"
    },
    {
      id:"3",
      rival: "Room 4",
      date: "11 May 2023 20:58",
      result: "win"
    },
    {
      id:"4",
      rival: "Room 5",
      date: "11 May 2023 22:02",
      result: "lost"
    }
  ]);

  return (
    <div className='d-flex justify-content-between'>
      <div className='blur wrap-history'>
      <h1 className='font-weight-bold'>This is your Game History</h1>
     
      {histories.map((aList) =>(
          <History rival={aList.rival} date={aList.date} result={aList.result}/>
      ))}
    </div>
    <div style={{margin:"50px"}}>
      <img src={historyImg} alt="history" width="500"/>
    </div>

    </div>
    
  )
}
