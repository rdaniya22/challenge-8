import React, { useState, useEffect } from 'react';
import { Outlet, Route, Routes } from 'react-router-dom';

import './App.css';
import Landing from './pages/Landing';
import Player from './pages/Player';
import CreateRoom from './pages/CreateRoom';
import InsideRoom from './pages/InsideRoom';
import GameHistory from './pages/GameHistory';
import Dashboard from './pages/Dashboard';


function App() {
 
  
  
   
    

  

  return <Routes>
      
      <Route path='/' element={<Landing/>}/>
      <Route path='/login' element={<Landing page="login"/>}/>
      <Route path='/register' element={<Landing page="register"/>}/>
      <Route path='/create-room' element={<CreateRoom/>}/>
      <Route path="/user">
          <Route index element={<Dashboard />} />
          <Route path="history" element={<GameHistory />} />
          <Route path="profile" element={<Player />} />
      </Route>
      <Route path='/room-1' element={<InsideRoom titleRoom="Room 1" player_1="harry_potter" player_2="draco_malfoy"/>}/>
      <Route path='/room-2' element={<InsideRoom titleRoom="Room 2" player_1="harry_potter" player_2="computer"/>}/>
      <Route path="*" element={<div><h1 style={{textAlign:"center"}}>SORRY, <br/>the page you are looking for, does not exist</h1></div>}/>

  </Routes>
}

export default App;
