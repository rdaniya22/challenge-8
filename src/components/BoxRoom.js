import React from 'react'

export default function BoxRoom(props) {
  return (
    <div className='boxRoom blur'>
      <h3>{props.roomNumber}</h3>
      <p>Winner: {props.winner}</p>
    </div>
  )
}
