const Input = (props) => {
  //props sama dengan attribute
  const classInput = `input ${props.newClass ? props.newClass : ''}`;
 return(
  
  <input className={classInput} type={props.type} placeholder={props.placeholder} onChange={props.onChange}/>
 );

}

export default Input;