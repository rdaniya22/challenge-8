import React from 'react'
import { Link } from 'react-router-dom'

export default function Nav() {
  return (
    <div className='outer-nav' style={{marginTop:"30px"}}>
      
      <div className='nav-wrap'>
        <ul>
          <li><Link to={"/login"}>Login</Link></li>
          <li><Link to={"/register"}>Registration</Link></li>
          <li><Link to={"/user"}>Dashboard</Link></li>
        </ul>

      </div>
    </div>
  )
}
