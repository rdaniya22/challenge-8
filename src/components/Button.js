const Button = (props) => {
  //props sama dengan attribute
  let buttonTitle = "Submit";
  if(props.title){
    buttonTitle = props.title;
  }

  const buttonClass = `button-submit ${props.addClass ? props.addClass : ''}`;
 return(

  <button className={buttonClass} onClick={props.onClick}>{buttonTitle}</button>
 );

}

export default Button;