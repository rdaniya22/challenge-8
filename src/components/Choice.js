import React from 'react'

export default function Choice(props) {
  return (
    <div className='choice-wrap blur'>
      <div className='choice'>{props.value}</div>
    </div>
  )
}
