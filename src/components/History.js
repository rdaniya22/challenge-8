import React from 'react'

export default function History(props) {
  return (
    <div className='d-flex justify-content-between'>
      <div className='line-history d-flex flex-row'>
          <div className='hist-rival font-weight-bold'>{props.rival}</div> <div  className='hist-date'>{props.date}</div> <div  className='hist-result font-weight-bold'>{props.result}</div>
      </div>
      
    </div>
  )
}
